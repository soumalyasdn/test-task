import { Router } from '@angular/router';
import { MainServiceService } from './../service/main-service.service';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
declare var jQuery: any;
@Component({
  selector: 'app-disease',
  templateUrl: './disease.component.html',
  styleUrls: ['./disease.component.scss']
})
export class DiseaseComponent implements OnInit , AfterViewInit{
  diseaseName = JSON.parse(localStorage.getItem(('Disease Name')));
 patient_id = localStorage.getItem("patient_id")
  myForm:FormGroup;
  formvalue: any
  desease: any
  mapName: any
 // state name group for sum of all excerice 
 mapData: any = {
  "PL-WP": 3211,
  "PL-SK": 3216,
  "PL-ZP": 3209,
  "PL-SL": 3213,
  "PL-PK": 3219,
  "PL-MZ": 3215,
  "PL-PD": 3217,
  "PL-LB": 3210,
  "PL-LD": 3214,
  "PL-KP": 3212,
  "PL-LU": 3218,
  "PL-OP": 3220,
  "PL-PM": 3207,
  "PL-DS": 3208,
  "PL-MA": 3221,
  "PL-WN": 3206
};
userId:any;
constructor(private router: Router,
            private fb: FormBuilder,
            private toastr: ToastrService,
            private mainService: MainServiceService) { }
ngOnInit() {
  this.mainService.userDataObservable.subscribe((res: any) => {
    console.log('userdata analyticse===',res)
  })
this.patient_id = localStorage.getItem("patient_id")

  let obj = {
  }
  jQuery(document).ready( () => {
    console.log('mapData 1===================',this.mapData);
    let self = this
 jQuery('#pl-map-exercise').vectorMap({
   map: 'pl_mill',
   labels: {
     regions: {
       render: function(code){
         return code.split('-')[1];
       },
     }
   },
   series: {
     regions: [{
       values: this.mapData,
       scale: ['#ffffff', '#F77D43'],
       normalizeFunction: 'polynomial'
     }]
   },
   onRegionClick:function(event, code) {                        
    var name = (code);                        
    console.log(name)    
    self.mapName = name              
    }
  });console.log('mapData 1',this.mapData)
  });
  this.desease = JSON.parse(localStorage.getItem("Disease Name"))
  console.log('mapData 1',this.mapData)
  this.myForm = this.fb.group({   
    gender: [''],
    dateOfBirth: [''], 
    disease: [this.desease ],
    state: ['']
    })
  } 
  ngAfterViewInit(){
  console.log('mapData 2',this.mapData)
  }
  mapeevnt(event) {
    console.log(event)
  }
  onSubmit() {
    console.log(this.myForm.value)
    this.myForm.value.state = this.mapName
    this.myForm.value.patientId = this.patient_id
    this.formvalue = this.myForm.value
      this.mainService.getUserDetails(this.formvalue).subscribe((res: any) => {
        if(res.code == 200) {
          console.log("User: Successfully logged in")
          this.toastr.success('Data saved successfully')
          this.router.navigate(['/login']);
        } else {
          this.toastr.error('Please try again later')
        }      
      })
  }
}
