import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  istoggle = false;
  checkFlag =true

  constructor(private router: Router) { }
  
  ngOnInit() {
    if(localStorage.getItem("patient_id")) {
      this.checkFlag = true
    } else {
      this.checkFlag = false
    }
  }

  toggleMenu(){
    this.istoggle = !this.istoggle;
  }
  onClickPacjent() {
    this.router.navigate(['/login']);
  }
  onClickOBK() {
    this.router.navigate(['/login']);
  }
  goToHome() {
    this.router.navigate(['/home']);
  }
}
