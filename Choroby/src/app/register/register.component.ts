import { MainServiceService } from './../service/main-service.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  myForm:FormGroup;
  public User: string[] = ["Hospital Admin", "Patient"];
  formvalue: any
  userType: any
  constructor(private router: Router,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private mainService: MainServiceService
    ) { }

  ngOnInit() {
    this.myForm = this.fb.group({  
      name: [''],        
      email: [''],
      mobile_no: [''],
      password: ['']
      })
  }
  onSelect(event) {
    console.log(event.target.value)
    this.userType = event.target.value
  }
  onSubmit() {
    console.log(this.myForm.value)
    this.myForm.value.role = this.userType
    this.formvalue = this.myForm.value
      this.mainService.getUserRegistered(this.formvalue).subscribe((res: any) => {
        if(res.code == 200) {
          console.log("User: Successfully logged in")
          this.toastr.success('Registered successfully')
          this.router.navigate(['/login']);
        }  else {
          this.toastr.error('Please try again later')
        }  
      })
  }
}
