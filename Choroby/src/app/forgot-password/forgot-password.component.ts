import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  myForm:FormGroup;
  constructor(private router: Router,
    private fb: FormBuilder) { }

  ngOnInit() {
    this.myForm = this.fb.group({          
      username: ['']
      })
  }
  onSubmit() {
    console.log(this.myForm.value)
    this.router.navigate(['/login']);
  }
  
}
