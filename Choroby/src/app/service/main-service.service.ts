import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

interface UserModelData   {
  firstName: string,
  lastName: string
  dob: string,
  email: string,
  gender: string,
  country: string,
  phone_numberCode: string,
  phone_number: string,
  userId: string,
  imageUrl: string,
}

@Injectable({
  providedIn: 'root'
})
export class MainServiceService {
  apiURL = environment.apiURL;
  private UserDataSubject = new BehaviorSubject<UserModelData>(null);
  
  private userLoginURL = this.apiURL +'user/login';
  private userRegisterURL = this.apiURL +'user/UserRegistration';
  private allDosesURl = this.apiURL +'allDoses';
  private userDetailURL = this.apiURL +'patientDetails';
  private hospitalListURL = this.apiURL +'hospitalList';
  private userDetailByIdURL = this.apiURL +'patientDetails/';

constructor(private http: HttpClient) { }

getUserLogin(data):Observable<any> {
  return this.http.post(this.userLoginURL, data);
}
getUserRegistered(data):Observable<any> {
  return this.http.post(this.userRegisterURL, data);
}
getAllDoses():Observable<any> {
  return this.http.get(this.allDosesURl);
}
getUserDetails(data):Observable<any> {
  return this.http.post(this.userDetailURL, data);
}
getUserDetailsById(id):Observable<any> {
  return this.http.get(this.userDetailByIdURL + id);
}
getHospitals():Observable<any> {
  return this.http.get(this.hospitalListURL);
}
userDataObservable = this.UserDataSubject.asObservable()
}
