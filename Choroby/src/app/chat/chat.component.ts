import { Component, OnInit } from '@angular/core';
import { MainServiceService } from '../service/main-service.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
// import { ChatService } from './chat.service';


@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
  formSMS: FormGroup;
  istoggleChat = false;
  hospitals: any;
  messages= [];
  temp: any;
  patient_id: string;
  baseurl: any;
  fullUrl: string;
  mainurl: string;
  url: string;
  constructor(private mainService: MainServiceService, private router: Router, 
    // private chatService:ChatService
    ) {
     this.baseurl = environment.baseurl;
     this.mainurl= this.baseurl + "chat/";
     console.log(this.mainurl)
    
  
    // this.chatService.newUserJoined().subscribe(data => {
    // });
    // this.chatService.userLeftRoom().subscribe(data => {
    // })
    this.mainService.getHospitals().subscribe((res: any) => {
     this.hospitals = res.data;
     console.log(this.hospitals)
      }        
    )
    this.patient_id= localStorage.getItem("patient_id")
    this.mainService.getUserDetailsById(this.patient_id).subscribe((res: any) => {
    console.log(res)
     })
   }

  ngOnInit() {
    this.formSMS = new FormGroup({
      message: new FormControl('')
    });
  }
  sendMessage() {
    this.temp = this.formSMS.value.message
  
this.messages.push(this.temp)
    console.log(this.messages)
    this.formSMS.reset()
  }
  clickHospital(){
 
    // this.mainurl= "http://localhost:4200/chat/";
    console.log()
    this.fullUrl = this.router.url
    this.url = this.fullUrl.split("/")[2];
 console.log("heya",this.url)

  }
  
  toggleChat() {
    this.istoggleChat = !this.istoggleChat;
  }

}
