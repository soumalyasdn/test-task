import { MainServiceService } from './../service/main-service.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listings',
  templateUrl: './listings.component.html',
  styleUrls: ['./listings.component.scss']
})
export class ListingsComponent implements OnInit {
  searchText: any
  allDoses: any
  patient_id: string;
  constructor(private router: Router,
    private mainService: MainServiceService) { }

  ngOnInit() {
    this.getAllDoses()
  }
  getAllDoses() {
    this.mainService.getAllDoses().subscribe((res: any) => {
      this.patient_id = localStorage.getItem("patient_id")
        console.log("getAllDoses", this.patient_id)
        this.allDoses = res.data.data
    })
  }
  onFolderSelect(event, value) {
    console.log("onFolderSelect: ", event.target.value, value)
    localStorage.setItem("Disease Name", JSON.stringify(value))
    this.router.navigate(['/disease']);
  }
}
