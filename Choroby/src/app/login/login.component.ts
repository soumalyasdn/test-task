import { MainServiceService } from './../service/main-service.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from "ngx-toastr";
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  myForm:FormGroup;
  formvalue: any
  userType: any
  patientId: void;
  constructor(private router: Router,
              private fb: FormBuilder,
              private toastr: ToastrService,
              private mainService: MainServiceService
              ) { }

  ngOnInit() {
    this.myForm = this.fb.group({          
      email: [''],
      password: ['']
      })
  }
  onSelect(event) {
    console.log(event.target.value)
    this.userType = event.target.value
  }
  onClickRegister() {
    this.router.navigate(['/register']);
  }
  onClickForgotPassword() {
    this.router.navigate(['/forgotpassword']);
  }
  onSubmit() {
    console.log(this.myForm.value)
    this.formvalue = this.myForm.value
   localStorage.removeItem("patient_id")
      this.mainService.getUserLogin(this.formvalue).subscribe((res: any) => {
        if(res.code == 200) {
          this.toastr.success('Login successfully')
          console.log("User: Successfully logged in", res.data)
          this.patientId = localStorage.setItem("patient_id",res.data.userInfo._id)
          this.router.navigate(['/listings']);
        }
        else{
          this.toastr.error('Please try again')
        }
      })
  }
}
