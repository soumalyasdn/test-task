const { request } = require('https');

module.exports = function (express) {
    const router = express.Router()
    // user
    require('./modules/user/user_routes')(router);
    require('./modules/all_doses/all_doses_routes')(router);
    require('./modules/patient_details/patient_details_routes')(router);
    require('./modules/hospital/hospital_routes')(router);
    require('./modules/patient/patient_routes')(router);
    
   // require('./modules/chat/chat_routes')(router);
    //require('./modules/video/video_routes')(router);
    require('./modules/v_chat/v_chat_routes')(router);
    // require('./modules/psychiatrist/psychiatrist_routes')(router);
    // require('./modules/icp/icp_routes')(router);
    // require('./modules/treatment/treatment_routes')(router);
    require('./modules/role/role_routes')(router);
    require('./modules/chat/chat_routes')(router);
    return router;
}
