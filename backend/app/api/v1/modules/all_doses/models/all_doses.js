const mongoose = require("mongoose");

const allDoses = new mongoose.Schema({
    doseName :{
        type:String,
        required:true,
        unique:true
    },
    companyId:{
        type:Number,
        required:true
    }
},{timestamps:true});

var alldoses = mongoose.model("AllDose",allDoses);
module.exports = alldoses;