module.exports = function (router) {
    var allDosesController = require('./controllers/all_doses_ctr');

    router.post('/allDoses',allDosesController.saveDoses);
    router.get('/allDoses',allDosesController.getDoses);
    return router;
}