const allDoses = require('../models/all_doses');
const mongoose = require("mongoose");
//const allDoses = mongoose.model("AllDose");
const constant = require("../../../../../../app/lib/constants")
const Response = require('../../../../../../app/lib/response')
const query    = require("../../../../../../app/lib/common_query")
const saveDoses = async(req, res) => {
    try{
        if (!req.body.companyId || !req.body.doseName) {
            res
            .status(constant.statusCode.validation)
            .json(Response(constant.statusCode.validatation,
                        "companyId and doseName are required"));
        }
        const stat = query.InsertIntoCollection(allDoses,req.body);
        if(stat){
            res
            .status(constant.statusCode.ok)
            .json(Response(constant.statusCode.ok,
                "Dosage saved successfully",
                stat))
        }
        else{
            res
             .status(constant.statusCode.validation)
             .json(Response(constant.statusCode.validatation,
                   "Could not save dosages"))            
        }
    }
    catch(err){
        status(constant.statusCode.validation)
             .json(Response(constant.statusCode.validatation,
                   "Could not save dosages"))
    }
  };
  
  const getDoses = async(req,res)=>{
      try{
      let doses = await query.findData(allDoses,{},{});
      //res.status(200).json({doses,message:"Dosages found successfully"});
      res
         .status(constant.statusCode.ok)
         .json(Response(constant.statusCode.ok,
                        constant.messages.doses_success,
                        doses));
      }
      catch(err){
          res
             .status(constant.statusCode.validation)
             .json(Response(constant.statusCode.validatation,
                   constant.messages.doses_error))
      }
  }
  module.exports = {
      saveDoses,
      getDoses
  };

