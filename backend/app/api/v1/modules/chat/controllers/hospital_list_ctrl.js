// function getHospitalData(req, res) {
//     async function listByIdAdminExe() {
    const Role = require('../../role/models/role_model');
    const User = require('../../user/models/user_model');
const constant = require("../../../../../../app/lib/constants")
const Response = require('../../../../../../app/lib/response')
const query    = require("../../../../../../app/lib/common_query")
    const getHospitalData = async(req,res)=>{
        try {
            console.log("req body===",req.body)
            let condition1 = {};
            // let findUserRoleId = await query.findoneData(Role, { name: constant.varibleType.HOSPITAL });
            let findUserRoleId = await query.findoneData(Role, { role: constant.varibleType.HOSPITAL });
           
            let userRoleId = '';
            console.log("user role id",findUserRoleId)
            if (findUserRoleId.status && findUserRoleId.data) {
                userRoleId = findUserRoleId.data._id;
                
            }
            
                condition1.role_id = userRoleId;
               
                condition1.isDeleted = false
                console.log("condition1====",userRoleId)
          
              
           
            // var fetchVal = { name:1, role_id: 1, isActive: 1, isDeleted: 1, _id: 1 };
            //
            var fetchVal = { name:1, role_id: 1, isActive: 1, isDeleted: 1, _id: 1 };
           
            let HospitalData = await query.findData(User, condition1,fetchVal);
             console.log("ManagerData.dataManagerData.data", HospitalData.data);
            if (HospitalData.status ) {
                res.json({
                    code: 200,
                    data: HospitalData.data,
                    message: constant.messages.Hospital_listData
                })
            }
            else {
                return res.json(Response(500, constant.validateMsg.internalError, result.error));
            }
        }
        catch (err) {
            return res.json(Response(500, constant.validateMsg.internalError, err));
        }

    }
//     listByIdAdminExe().then(function (data) { });
// }
const getHospitalDataById = async(req,res)=>{
    try {
        console.log("req body===",req.body)
       
        //
        // let HospitalData =  await User.findOne({_id: req.params.id});
        let HospitalData =  await User.find();
       
        // let HospitalData = await query.findData(User, condition1,fetchVal);
         console.log("ManagerData.dataManagerData.data", HospitalData.data);
        if (HospitalData.status ) {
            res.json({
                code: 200,
                data: HospitalData.data,
                message: constant.messages.Hospital_listData
            })
        }
        else {
            return res.json(Response(500, constant.validateMsg.internalError, result.error));
        }
    }
    catch (err) {
        return res.json(Response(500, constant.validateMsg.internalError, err));
    }

}
module.exports = {
   getHospitalData,
   getHospitalDataById
}
