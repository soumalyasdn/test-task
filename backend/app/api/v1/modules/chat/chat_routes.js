module.exports = function (router) {
    var chat = require("../../../../lib/chat");
    var hospitalList = require('./controllers/hospital_list_ctrl');
    router.post("/get-all-chat", chat.getAllMessage);
    router.get('/hospitalList',hospitalList.getHospitalData);
    router.get('/hospitals/:id',hospitalList.getHospitalDataById);

    return router;
}