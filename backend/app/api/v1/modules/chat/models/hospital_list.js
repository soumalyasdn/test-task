const mongoose = require("mongoose");

const hospitalSchema = new mongoose.Schema({
   
    role_id:{type:mongoose.Schema.Types.ObjectId,ref:'Role',index:true,required:true},
   
    user_id: { type: mongoose.Schema.Types.ObjectId, ref:'user'},
},
{timestamps:true});

var hospitalLists = mongoose.model("HospitalLists",hospitalSchema);
module.exports = hospitalLists;



