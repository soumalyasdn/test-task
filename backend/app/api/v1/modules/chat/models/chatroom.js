var mongoose = require("mongoose");

var chatroomschema = mongoose.Schema(
    {
        from_id: {
            type: mongoose.Types.ObjectId,
            ref: "user"
        },
        to_id: {
            type: mongoose.Types.ObjectId,
            ref: "user"
        },
        from_name: {
            type: String,
            required: false,
            default: null
        },
        to_name: {
            type: String,
            required: false,
            default: null
        },
        status: {
            type: Boolean,
            required: false,
            default: false
        },
    },

    {
        timestamps: true
    }
);
var chatrooms = (module.exports = mongoose.model("chatrooms", chatroomschema));
