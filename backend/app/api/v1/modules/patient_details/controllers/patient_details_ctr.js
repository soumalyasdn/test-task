const mongoose = require("mongoose");
const patientDetails = require('../models/patient_details')
const constant = require("../../../../../../app/lib/constants")
const Response = require('../../../../../../app/lib/response')
const query    = require("../../../../../../app/lib/common_query")
const savePatientDetails = async(req, res) => {
    try{
        if (!req.body.patientId) {
            res
            .status(constant.statusCode.validation)
            .json(Response(constant.statusCode.validatation,
                        "PatientId is required"));
        }
        console.log(req.body)
        const stat = await query.InsertIntoCollection(patientDetails,req.body);
        if(stat){
            res
            .status(constant.statusCode.ok)
            .json(Response(constant.statusCode.ok,
                "Patient details saved successfully",
                stat))
        }
        else{
            res
             .status(constant.statusCode.validation)
             .json(Response(constant.statusCode.validatation,
                   "Could not save patient details"))            
        }
    }
    catch(err){
        console.log(err);
        res
          .status(constant.statusCode.validation)
          .json(Response(constant.statusCode.validatation,
                   "Could not save patient details"))
    }
  };
  
  const getPatientDetails = async(req,res)=>{
      try{
      let details = await query.findData(patientDetails,{},{});
      //res.status(200).json({doses,message:"Dosages found successfully"});
      res
         .status(constant.statusCode.ok)
         .json(Response(constant.statusCode.ok,
                        "Patient details fetched successfully",
                        details));
      }
      catch(err){
          console.log(err);
          res
             .status(constant.statusCode.validation)
             .json(Response(constant.statusCode.validatation,
                   "Could not fetch patient details"))
      }
  }
  const getPatientDetailsById = async(req,res)=>{
    try{
    let detail = await patientDetails.findOne({patientId: req.params.id});
    //res.status(200).json({doses,message:"Dosages found successfully"});
    res
       .status(constant.statusCode.ok)
       .json(Response(constant.statusCode.ok,
                      "Patient details fetched successfully",
                      detail));
    }
    catch(err){
        console.log(err);
        res
           .status(constant.statusCode.validation)
           .json(Response(constant.statusCode.validatation,
                 "Could not fetch patient details"))
    }
}

  module.exports = {
      savePatientDetails,
      getPatientDetails,
      getPatientDetailsById
  };

