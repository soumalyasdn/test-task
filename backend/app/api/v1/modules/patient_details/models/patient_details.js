const mongoose = require("mongoose");

const patientDetails = new mongoose.Schema({
    patientId:{
        type:mongoose.Types.ObjectId,ref:'user'
       
     
    },
    disease:{
        type:String,
        required:true
    },
    gender: {
        type: String,
        enum: ["Męski", "Płeć żeńska","Inny"]
    },
    dateOfBirth: {
       type: Date
    },
    state:{
        type:String
    }
},{timestamps:true});

var patientDetails1 = mongoose.model("patientDetails",patientDetails);
module.exports = patientDetails1;
