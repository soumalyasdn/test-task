module.exports = function (router) {
    var patientDetailsController = require('./controllers/patient_details_ctr');

    router.post('/patientDetails',patientDetailsController.savePatientDetails);
    router.get('/patientDetails',patientDetailsController.getPatientDetails);
    router.get('/patientDetails/:id',patientDetailsController.getPatientDetailsById);

    return router;
}
