'use strict';

var mongoose = require('mongoose');
// var constantsObj = require('../../../../../lib/constants');

var roleSchema = new mongoose.Schema({

    role: { type: String,default : ''},
    isDeleted: { type: Boolean, default: false },
    isActive:{ type:Boolean,default:true },
    //createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
}, {
        timestamps: true
    });

var Role = mongoose.model('Role', roleSchema);
module.exports = Role;

