'use strict';

var mongoose = require('mongoose'),
    async = require('async'),
    // validator = __rootRequire('app/lib/validator'),
    Error = __rootRequire('app/lib/error.js'),
    // Response = __rootRequire('app/lib/response.js'),
    constant = __rootRequire('app/lib/constants'),
    query = __rootRequire('app/lib/common_query'),
    Role = mongoose.model('Role');

const config = __rootRequire('app/config/config.js').get(process.env.NODE_ENV);
// const stripe = require("stripe")("sk_test_a0kdl6EVFQiXr73mBgbr3esW00xTBQCYLL");

module.exports = {
    addEditRole: addEditRole,
    getAllRole:getAllRole,
    changeStatus:changeStatus,
};

/*
    Desc:Function to add/edit role 
    Date:03-Dec-2019
    Created By : vikash kumar
    method : post
*/
function addEditRole(req, res) {
    console.log("req=================");
    async function add_role() {
        if (req.body && (!req.body.name)) {
            res.jsonp(
                Error(
                    constant.statusCode.error, 
                    constant.role_message.casetype_name, 
                 )
             );
         }else {
            var data = {};
            var userData = {};
            data.name = req.body.name;
            var msg = '';
            var cond = {name:req.body.name};
            var Fields = {_id:1,name:1}
            if(req.body.id==''){
                var chkRole = await query.findoneData(Role, cond, Fields);
                if(chkRole.data){
                    res.json({
                        code: 200,
                        data: chkRole.data,
                        message: constant.role_message.roleAlreadyExist
                    })
                }else{
                    var saveObj = await query.uniqueInsertIntoCollection(Role, data);
                    msg = constant.role_message.role_added_successfully;
                    userData = saveObj.userData;
                }                
            }else{
                var condition = {_id:req.body.id}
                var saveObj = await query.updateOneDocument(Role, condition, data);
                msg = constant.role_message.role_updated_successfully;
                userData = saveObj.data;
            }
            if(saveObj.status==true){
                res.json({
                    code: 200,
                    data: userData,
                    message: msg
                })
            }else{
                res.json({
                    code: 500,
                    message: constant.messages.requestNotProcessed
                })
            }                   
        }
    }
    add_role().then(function (data) { 

    });
}
/*
    Desc:Function to get all role
    Date:03-Dec-2019
    Created By : vikash kumar
    method : get
*/
function getAllRole(req,res){
    console.log("req===========sssssssssss");
    async function get_roles(){
        var condition = {isDeleted:false};
        var Fields = {name:1,isActive:1,isDeleted:1,_id:1};
        let roleObj = await query.findData(Role, condition, Fields);
        if(roleObj.status==true){
            if(roleObj.data.length > 0){
                res.json({
                    code: 200,
                    data: roleObj.data,
                    message: constant.role_message.role_list_success
                })
            }else{
                res.json({
                    code: 200,
                    data: [],
                    message: constant.validateMsg.noRecordFound
                })
            }
        }else{
            res.json({
                code: 500,
                message: constant.messages.requestNotProcessed
            })
        } 
    }
    get_roles().then(function (data){

    })
}

/*
    Desc:Function to change status of  caseType and delete caseType
    Date:03-Dec-2019
    Created By : vikash kumar
    method : post
*/
function changeStatus(req,res){
    async function change_status(){
        if (req.body && (req.body.status =='')) {
            res.jsonp(
                Error(
                    constant.statusCode.error, 
                    constant.message.status_required, 
                 )
             );
        }else {
            var data = {};
            var msg = '';
            if(req.body.status){
                data.isActive = req.body.status=='true'?false:true;
                msg = constant.role_message.role_status_updated;
            }else{
                data.isDeleted = req.body.delete=='true'?false:true;
                msg = constant.role_message.role_status_deleted
            }
            var condition = {_id:req.body.id}
            let updateObj = await query.updateOneDocument(Role, condition, data);
            if(updateObj.status==true){
                res.json({
                    code: 200,
                    data: updateObj.data,
                    message: msg
                })
            }else{
                res.json({
                    code: 500,
                    message: constant.messages.requestNotProcessed
                })
            } 
        }
    }
    change_status().then(function(data){

    })
}
