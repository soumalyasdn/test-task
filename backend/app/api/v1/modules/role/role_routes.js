module.exports = function (router) {
    
    var utils = __rootRequire('app/lib/util');
    var middlewares = [utils.ensureAuthorized];
    
    var role = require('./controllers/role_ctrl');
    
    router.post('/role/addEditRole', role.addEditRole); 
    router.get('/role/getAllRole',middlewares, role.getAllRole); 
    router.post('/role/changeStatus',middlewares,role.changeStatus);
    router.post('/role/deleteRole',middlewares,role.changeStatus);
    return router;
}