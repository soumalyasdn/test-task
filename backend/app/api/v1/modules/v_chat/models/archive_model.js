
'use strict';

var mongoose = require('mongoose');

var archiveSchema = new mongoose.Schema({
    patientId: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    psychiatristId: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    archiveArray: [{
        video_url: {
            type: String,
            required: false
        },
        archiveId: {
            type: String,
            required: false
        }
    }],
}, {
    timestamps: true
});
var archive = mongoose.model('archive', archiveSchema);
module.exports = archive;
