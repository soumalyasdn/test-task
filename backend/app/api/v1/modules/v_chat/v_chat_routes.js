module.exports = function (router) {

    var utils = require('../../../../lib/util');
    var middlewares = [utils.ensureAuthorized];

    var chat = require('./controllers/video_conferencing_ctrl');

    router.post('/chat/generatesessionId', chat.generateSessionId);
    router.post('/chat/generatesessionToken', chat.generateSessionToken);
    router.post('/chat/disconnectSession', chat.forceDisconnectSession);
    router.post('/chat/setuserBusy', chat.setUserAsBusy);
    router.post('/chat/setuserAvailable', chat.setUserAsAvailable);
    router.post('/chat/getCallstatus', chat.getUsersCallStatus);
    router.post('/chat/startarchiveCall', chat.startArchiveCall);
    router.post('/chat/stoparchiveCall', chat.stopArchiveCall);
    router.post('/chat/getArchive', chat.getArchive);
    router.post('/chat/getsavedArchiveData', chat.getsavedArchiveData);   
    router.post('/chat/saveArchive', chat.saveArchive);    

    return router;
}