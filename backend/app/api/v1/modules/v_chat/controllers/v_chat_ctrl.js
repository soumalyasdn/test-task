User = require('../../user/models/user_model');
query = require('../../../../../../app/lib/common_query'),

    module.exports = {
        socketEvents: socketEvents
    };

function socketEvents(io) {
    console.log("connected socket--------------")

    io.on("connection", async (socket) => {
        console.log("socket Id is---------------", socket.id)
        socket.on("loggedinuserId", async (userdata) => {
            console.log("userdatta", userdata)
            try {
                let userobject = {
                    socketId: socket.id
                }
                let cond = { _id: userdata.userId };
                let result = await query.updateOneDocument(User, cond, userobject);
                console.log("socketId updated", result)
                io.to(`${socket.socketId}`).emit('sessionId', socket.socketId);
            } catch (error) {
                console.log("err from socket", socket)
            }
        })

        socket.on("sessionuserdata", async (getSessiondata) => {
            let user = await User.findOne({
                _id: getSessiondata.userId
            })
            if (user) {
                io.to(`${user.socketId}`).emit('sessionId', getSessiondata.sessionId);
            }
        })

        socket.on("sessionPatientData", async (getPatientdata) => {
            console.log("getPatientdata", getPatientdata)
            let user = await User.findOne({
                _id: getPatientdata.userId
            })
            if (user) {
                io.to(`${user.socketId}`).emit('patientId', getPatientdata.patientId);
            }
        })

        socket.on("loggedinuserId", async (getuserId) => {
            let user = await User.findOne({
                _id: getuserId.userId
            })
            if (user) {
                io.to(`${user.socketId}`).emit('socketdata', user.socketId);
            }
        })

        socket.on("rejectCall", async (getPatientdata) => {
            console.log("rejectCalldata", getPatientdata)
            let patient = await User.findOne({
                _id: getPatientdata.patientId
            })
            if (patient.socketId) {
                io.to(`${patient.socketId}`).emit('callRejected', getPatientdata.message);
            }
        })

        socket.on("callDisconnected", async (getObj) => {
            console.log("getObj", getObj)
            let user = await User.findOne({
                _id: getObj.userId
            })
            if (user) {
                io.to(`${user.socketId}`).emit('sessionDisconnect', getObj.message);
            }
        })

    })
}