
'use strict';

const util = require('util'),
    validator = require('../../../../../../app/lib/validator'),
    jwt = require('jsonwebtoken'),
    utility = require('../../../../../../app/lib/utility.js'),
    Error = require('../../../../../../app/lib/error.js'),
    Response = require('../../../../../../app/lib/response.js'),
    constant = require('../../../../../../app/lib/constants'),
    query = require('../../../../../../app/lib/common_query'),
    Role = require('../../role/models/role_model'),
    User = require('../../user/models/user_model'),
    Archive = require('../models/archive_model');

module.exports = {
    generateSessionId: generateSessionId,
    generateSessionToken: generateSessionToken,
    forceDisconnectSession: forceDisconnectSession,
    setUserAsBusy: setUserAsBusy,
    getUsersCallStatus: getUsersCallStatus,
    setUserAsAvailable: setUserAsAvailable,
    startArchiveCall: startArchiveCall,
    stopArchiveCall: stopArchiveCall,
    getArchive: getArchive,
    saveArchive: saveArchive,
    getsavedArchiveData: getsavedArchiveData
}

var OpenTok = require('opentok'),
    // opentok = new OpenTok('46643412', 'ab646d139c58e77e0a2cdb90eeebbaa0bea95ce1');

    opentok = new OpenTok('46629662', '169538890c6c8c57097cc3260392f0f3602c66d1');

// 46643412 ab646d139c58e77e0a2cdb90eeebbaa0bea95ce1
/**
 * Generate OpenTok Session Id
 * @access private
 * @return json
 * Created by Trisha
 * @smartData Enterprises (I) Ltd
 * Created Date 
 */


function generateSessionId(req, res) {
    console.log("sessionId")
    async function generateSessionId() {
        try {
            opentok.createSession({ mediaMode: "routed" }, (err, session) => {
                if (err) {
                    res.json({
                        code: 400,
                        message: 'Error occured'
                    })
                    console.log('err on creating session', err);
                }
                let token = opentok.generateToken(session.sessionId, { expireTime: (new Date().getTime() / 1000) + (1 * 60 * 1000) }); // expires in 30 minutes
                console.log("sessionId", session.sessionId, "token", token)
                res.json({
                    code: 200,
                    message: 'Created successfully',
                    token: token,
                    sessionId: session.sessionId
                })
            });
        } catch (error) {
            console.log('error on generateSessionId main', error)
            res.json({
                code: 400,
                message: 'Something wrong'
            })
        }
    } generateSessionId();
}

/**
* Generate OpenTok Session Token
* @access private
* @return json
* Created by Trisha
* @smartData Enterprises (I) Ltd
* Created Date 
*/

function generateSessionToken(req, res) {
    console.log("token", req.body)
    async function generateSessionToken() {
        try {
            let token = opentok.generateToken(req.body.sessionId);
            console.log("user_token---------", token)
            res.json({
                code: 200,
                token: token,
                message: 'Success'
            })
        } catch (error) {
            console.log('error on generateSessionToken main', error)
            res.json({
                code: 400,
                message: 'Something wrong'
            })
        }
    } generateSessionToken();
}
function startArchiveCall(req, res) {
    async function startArchiveCall() {
        try {
            console.log("req.body archive", req.body.sessionId)
            let sessionId = req.body.sessionId
            opentok.startArchive(sessionId, { name: 'Patient call record' }, function (err, archive) {
                if (err) {
                    return console.log(err);
                } else {
                    // The id property is useful to save off into a database
                    console.log("new archive:" + archive.id);
                    console.log("archive", archive)
                    res.json({
                        code: 200,
                        message: 'Archiving started',
                        archive_id: archive.id,
                    })
                }
            });
        } catch (error) {
            console.log('error on generateSessionToken main', error)
            res.json({
                code: 400,
                message: 'Something wrong'
            })
        }
    } startArchiveCall();
}
function stopArchiveCall(req, res) {
    console.log("stop acrchive req.body", req.body.archiveId)
    let archiveId = req.body.archiveId
    opentok.stopArchive(archiveId, function (err, archive) {
        if (err) return console.log(err);
        console.log("Stopped archive:")
        res.json({
            code: 200,
            message: 'Archiving stopped',
            archive_data: archive,
        })
    });
}
function getArchive(req, res) {
    async function getArchive() {
        try {
            console.log("req.body get archive", req.body.archiveId)
            let archiveId = req.body.archiveId
            opentok.getArchive(archiveId, async function (err, archive) {
                if (err) return console.log(err);
                console.log("get archive", archive);
                console.log("get archive", archive.url);
                let video_url = await utility.encrypt(archive.url);
                console.log("video_url", video_url)
                res.json({
                    code: 200,
                    message: 'Archive url',
                    archive_data: archive,
                    video_url: video_url
                })
                // let obj = {
                //     // patientId: '5e8e852e7b5cdb16142b8a90',
                //     // psychiatristId: '5e874cdfc5d95c951d3538a7',
                //     patientId: req.body.patientId,
                //     psychiatristId: req.body.psychiatristId,
                //     archive_data: archive
                // }
                // let showData = await saveArchive(obj);
                // console.log("showdata-----------------", showData)
            });
        } catch (error) {
            console.log('error ', error)
            res.json({
                code: 400,
                message: 'Something wrong'
            })
        }
    } getArchive();
}


function saveArchive(req, res) {
    console.log("req----save", req)
    async function saveArchive() {

        try {
            // var ArcFinalData = []
            let condition_exist = {
                patientId: req.body.patientId,
                psychiatristId: req.body.psychiatristId
            }
            let data_exist = await query.findoneData(Archive, condition_exist)
            console.log("data_exist-------------12345", data_exist)
            if (data_exist.data != null) {
                console.log(data_exist, "dataexist---")
                let archive_array = []
                let videoUrl = await utility.encrypt(req.body.archive_url),
                    archiveId = await utility.encrypt(req.body.archive_id)
                var obj = {
                    video_url: videoUrl,
                    archiveId: archiveId
                }
                console.log("object", obj)
                archive_array.push(obj)
                console.log("archive_array---", archive_array)
                let obj_update = {
                    "archiveArray": archive_array
                }
                let updateCondition = {
                    _id: data_exist.data.id
                };
                console.log("obj_update", obj_update, updateCondition)
                Archive.findOneAndUpdate(updateCondition, {
                    $push: obj_update
                }).exec(function (err, archiveData) {
                    console.log("updateddata")
                    res.json({
                        code: 200,
                        message: 'Archive data saved',
                        // archiveData: archiveData,
                        video_url: videoUrl
                    })

                })

            }
            else {
                console.log("elsepart", req.body)
                let archive_array = []
                let videoUrl = await utility.encrypt(req.body.archive_url),
                    archiveId = await utility.encrypt(req.body.archive_id)
                let obj = {
                    video_url: videoUrl,
                    archiveId: archiveId
                }
                archive_array.push(obj)
                console.log(archive_array, "archive_array")
                let add_obj = {
                    "patientId": req.body.patientId,
                    "psychiatristId": req.body.psychiatristId,
                    "archiveArray": archive_array
                }
                console.log(add_obj, "add_obj")
                let archiveData = await query.InsertIntoCollection(Archive, add_obj);
                // return res.json(Response(200,'Archive data saved', archiveData));
                res.json({
                    code: 200,
                    message: 'Archive data saved',
                    archiveData: archiveData,
                    video_url: videoUrl
                })
            }

        } catch (error) {
            console.log('error ', error)
            res.json({
                code: 400,
                message: 'Something wrong'
            })
        }

    } saveArchive();
}


function getsavedArchiveData(req, res) {
    async function asy_init() {
        try {
            let condition = {
                patientId: req.body.patientId,
                psychiatristId: req.body.psychiatristId
            }
            Archive.findOne(condition).populate({
                path: 'patientId',
                select: 'userName',
            }).populate({
                path: 'psychiatristId',
                select: 'userName',
            }).exec(async function (err, ArchiveData) {
                console.log("ArchiveData", ArchiveData)
                return res.json({
                    code: 200,
                    message: 'Archive data fetched',
                    data: ArchiveData,
                })
            })
        } catch (e) {
            return res.json({
                code: Constant.ERROR_CODE,
                message: Constant.SOMETHING_WENT_WRONG
            });
        }
    }
    asy_init();
}





/**
* Update user call status as busy
* @access private
* @return json
* Created by Trisha
* @smartData Enterprises (I) Ltd
* Created Date 
*/

function setUserAsBusy(req, res) {
    async function setUserAsBusy() {
        try {
            console.log('req.body', req.body)
            let dataSet = {
                callStatus: 'busy'
            }
            let query_p = {
                _id: req.body.publisher_id
            }
            let query_s = {
                _id: req.body.subscriber_id
            }
            let result_p = await query.updateOneDocument(User, query_p, dataSet);
            if (result_p) {
                let result_s = await query.updateOneDocument(User, query_s, dataSet);
                return res.json(Response(constant.statusCode.ok, constant.messages.Callstatus));

            } else {
                return res.json(Response(constant.statusCode.internalservererror, constant.validateMsg.internalError + " " + error));

            }
        } catch (error) {
            console.log('error on setUserAsBusy main', error)
            return res.json(Response(constant.statusCode.internalservererror, constant.validateMsg.internalError + " " + error));

        }
    } setUserAsBusy();
}

/**
* Get user Call status
* @access private
* @return json
* Created by Trisha
* @smartData Enterprises (I) Ltd
* Created Date 
*/

function getUsersCallStatus(req, res) {
    async function getUsersCallStatus() {
        try {
            let fetch_cond = {
                _id: req.body.subscriber_id
            }
            let data = await query.findoneData(User, fetch_cond, { _id: 1, callStatus: 1 });
            console.log('data on getUsersCallStatus', data)
            if (data) {
                if (data.callStatus == 'busy') {
                    return res.json(Response(constant.statusCode.failed, constant.user_busy));
                } else {
                    res.json({
                        code: constant.statusCode.ok,
                        message: constant.messages.user_available,
                        data: data
                    })
                }
            } else {
                res.json({
                    code: Constant.statusCode.notFound,
                    message: Constant.messages.userNotFound
                })
            }
        } catch (error) {
            console.log('error on getUsersCallStatus main', error)
            return res.json(Response(constant.statusCode.validation, constant.messages.commonError));
        }
    } getUsersCallStatus();
}

/**
* Update user call status as available
* @access private
* @return json
* Created by Trisha
* @smartData Enterprises (I) Ltd
* Created Date 
*/


function setUserAsAvailable(req, res) {
    async function setUserAsAvailable() {
        try {
            console.log('req.body', req.body)
            let dataSet = {
                callStatus: 'available'
            }
            let query_p = {
                _id: req.body.publisher_id
            }
            let query_s = {
                _id: req.body.subscriber_id
            }
            let result_p = await query.updateOneDocument(User, query_p, dataSet);
            if (result_p) {
                let result_s = await query.updateOneDocument(User, query_s, dataSet);
                return res.json(Response(constant.statusCode.ok, constant.messages.Callstatus));

            } else {
                return res.json(Response(constant.statusCode.internalservererror, constant.validateMsg.internalError + " " + error));

            }
        } catch (error) {
            console.log('error on setUserAsBusy main', error)
            return res.json(Response(constant.statusCode.internalservererror, constant.validateMsg.internalError + " " + error));

        }
    } setUserAsAvailable();
}

/**
* Disconnect ongoing Session
* @access private
* @return json
* Created by Trisha
* @smartData Enterprises (I) Ltd
* Created Date 
*/

function forceDisconnectSession(req, res) {
    async function forceDisconnectSessionExe() {
        try {
            console.log("\n\n forcedisconnect sessionId ", req.body.sessionId, req.body.connectionId);

            opentok.forceDisconnect(req.body.sessionId, req.body.connectionId, function (error, sucesssObj) {
                if (error) {
                    return res.json(Response(constant.statusCode.internalservererror, constant.validateMsg.internalError + " " + error));
                }
                else {
                    console.log("\n\n forcedisconnect sucesssObj ", sucesssObj);
                    res.json({
                        code: constant.statusCode.ok,
                        data: sucesssObj,
                        message: constant.messages.success
                    })
                }

            });
        } catch (error) {
            console.log('error on forceDisconnectSession main', error)
            return res.json(Response(constant.statusCode.validation, constant.messages.commonError));

        }
    }
    forceDisconnectSessionExe();
}
