'use strict';

var mongoose = require('mongoose');

var patientSchema = new mongoose.Schema({
    user_id: { type: mongoose.Schema.Types.ObjectId, ref:'user'},
    hospital_id: { type: mongoose.Schema.Types.ObjectId, ref:'hospital' },
    
    createdDate: { type: Date, default: Date.now },
    modifiedDate: { type: Date, default: Date.now },
}, {
        timestamps: true
    });

var patient = mongoose.model('patient', patientSchema);
module.exports = patient;