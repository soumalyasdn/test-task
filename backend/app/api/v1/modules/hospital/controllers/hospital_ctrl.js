'use strict';

const util = require('util'),
    validator = require('../../../../../../app/lib/validator'),
    jwt = require('jsonwebtoken'),
    utility = require('../../../../../../app/lib/utility.js'),
    mailer = require('../../../../../../app/lib/mailer.js'),
    Error = require('../../../../../../app/lib/error.js'),
    Response = require('../../../../../../app/lib/response.js'),
    constant = require('../../../../../../app/lib/constants'),
    query = require('../../../../../../app/lib/common_query'),
    messageTemplates = require("../../../../../lib/messagetemplates"),
    Role = require('../../role/models/role_model'),
    Hospital = require('../models/hospital_model'),
    User = require('../../user/models/user_model');

const config = require('../../../../../../app/config/config.js').get(process.env.NODE_ENV);

module.exports = {
    addHospital:addHospital,
    listHospital:listHospital,
   
}

/**
 * add hospital by Super Admin
 * @access private
 * @return json
 * Created by vikash kumar
 * @smartData Enterprises (I) Ltd
 * Created Date 20-Mar-2020
 */
function addHospital(req, res) {
    async function addHospitalMethod() {
        try {
            console.log("req body=============", req.body);
            let userRoleId = '';
            let findUserRoleId = await query.findoneData(Role, {
                name: constant.varibleType.HOSPITAL
            });
            if (findUserRoleId.status && findUserRoleId.data) {
                userRoleId = findUserRoleId.data._id;
            }
            let hospitalNo = Math.floor((Math.random() * 100000) + 1);
            console.log("hospitalNo",hospitalNo)
            //let SupervisorId = mongoose.Types.ObjectId(req.body.supervisor_id);
            let firstName = req.body.firstName ? req.body.firstName : ''
            let lastName = req.body.lastName ? req.body.lastName : '';
            let userName = firstName + ' ' + lastName;
            let userobject = {
                firstName: req.body.firstName ? req.body.firstName : '',
                lastName: req.body.lastName ? req.body.lastName : '',
                userName: req.body.userName ? req.body.userName : userName,
                email: req.body.email ? req.body.email : '',
                mobile_no: req.body.phone ? req.body.phone : '',
                password: req.body.password ? req.body.password : '',
                address: req.body.address ? req.body.address : '',
                city: req.body.city ? req.body.city : '',
                role_id: userRoleId ? userRoleId : '',
                hospitalName:req.body.hospitalName ? req.body.hospitalName : '',
                //station: req.body.station ? req.body.station : '',
                //isActive: true,
                createdby_id: req.body.createdby_id ? req.body.createdby_id : '',
                //manager_id: req.body.manager_id ? req.body.manager_id : ''
            }
            // userobject.resetkey = utility.uuid.v1();
            let baseUrl = config.baseUrl;
            let checkExistCond = { "email": req.body.email }
            let isExist = await query.findoneData(User, checkExistCond, {
                _id: 1
            });
            if (isExist.data && isExist.status) {
                return res.json({
                    code: constant.statusCode.validation,
                    message: constant.messages.emailAlreadyExist
                });
            } else {
                let addUserObject = await query.uniqueInsertIntoCollection(User, userobject);
                if (addUserObject.status) {
                    if (addUserObject.userData) {
                        console.log("addUserObject.userData", addUserObject.userData)
                        let userobject1 = {
                            user_id: addUserObject.userData._id ? addUserObject.userData._id : '',
                            hospital_no:hospitalNo,
                            description:req.body.description ? req.body.description : ''
                        }
                        let addUserHospital = await query.uniqueInsertIntoCollection(Hospital, userobject1);
                        console.log("userobject1userobject1", addUserHospital);

                        //for send message function

                        var userMailData = {
                            userId: addUserObject.userData._id,//userObj.data._id,
                            email: addUserObject.userData.email ? addUserObject.userData.email : '',
                            firstName: addUserObject.userData.firstName ? addUserObject.userData.firstName : '',
                            lastName: addUserObject.userData.lastName ? addUserObject.userData.lastName : '',
                            password: req.body.password ? req.body.password : '',
                            loginlink: baseUrl + '#/login',
                            //userName: addUserObject.userData.userName ? addUserObject.userData.userName : '',
                            //link: baseUrl + '#/create-password/' + updateKey.data.resetkey,
                        };
                        let obj = {
                            data: userMailData,
                            mailType: constant.varibleType.HOSPITAL,  //"hospital welcome"
                        }
                        console.log("data for manager welcome ==========", obj)
                        //let sendMail = await sendEmailFunction(obj);
                        let sendMail = await query.sendEmailFunction(obj)
                        if (sendMail) {
                            console.log("sendMailsendMail==========")
                            res.json(
                                Response(
                                    constant.statusCode.ok,
                                    constant.messages.hospitalWelcomeSuccess,
                                    //addUserObject.userData
                                )
                            );
                        }
                        else {
                            res.json(
                                Response(
                                    constant.statusCode.internalservererror,
                                    constant.validateMsg.internalError
                                )
                            );
                        }
                        //end===============

                        //return res.json(Response(constant.statusCode.ok, constant.messages.adminGroomer_created, addUserObject.userData));


                    } else {
                        return res.json(Response(constant.statusCode.internalservererror, constant.validateMsg.internalError));
                    }
                } else {
                    return res.json(Response(constant.statusCode.internalservererror, constant.validateMsg.internalError));
                }
            }
        } catch (error) {
            return res.json(Response(constant.statusCode.internalservererror, constant.validateMsg.internalError + " " + error));
        }
    }
    addHospitalMethod().then(function (data) {
    });
}


/**
 * List Hospital on Super Admin dashboard
 * @access private
 * @return json
 * Created by vikash kumar
 * @smartData Enterprises (I) Ltd
 * Created Date 24-Mar-2020
 */

function listHospital(req, res) {
    async function listHospital() {
        try {
            console.log("req.body",req.body)
            let condition ={
            "isDeleted" : false};
        //    var a= utility.getEncryptText('vikash')
        
        //    var b =utility.getDecryptText('1287df913279')
        //    console.log("aaaaaabbbbbbbbbbbbbbbbbbbbbbbbaa",b)

            let count = req.body.count ? parseInt(req.body.count) : 20;
            let skip = 0;
            if (req.body.count && req.body.page) {
                skip = req.body.count * (req.body.page - 1);
            }
            let searchText = decodeURIComponent(req.body.searchText).replace(/[[\]{}()*+?,\\^$|#\s]/g, "\\s+");
            let sortObject = {}
            if (req.body.sortValue && req.body.sortOrder) {
                sortObject[req.body.sortValue] = req.body.sortOrder;
            } else {
                sortObject = { _id: -1 }
            }
            //let condition = { isDeleted: false }
            if (req.body.searchText) {
                condition.$or = [
                    { 'userName': new RegExp(searchText, 'gi') },
                    { 'lastName': new RegExp(searchText, 'gi') },
                    { 'email': new RegExp(searchText, 'gi') },
                    { 'address': new RegExp(searchText, 'gi') },
                    { 'city': new RegExp(searchText, 'gi') },
                    {'uniquePatientId':new RegExp(searchText, 'gi')},
                    {'mobile_no':new RegExp(searchText, 'gi')}
                    
                ];
            }
            User.aggregate([
                {
                    $match: condition
                },
                // {
                //     $limit: count
                // },
                // {
                //     $skip: skip
                // },
                // {$sort: sortObject},
                {
                    $lookup:
                    {
                        from: "hospitals",
                        localField: "_id",
                        foreignField: "user_id",
                        as: "userdata"
                    }
                },
                {
                    $unwind: "$userdata"
                },          
                 {
                    $group: {
                      // _id: {"userName":"$userName"},
                        hospital_no:{"$first":"$userdata.hospital_no"},
                        userName:{"$first":"$userName"},
                         description:{"$first":"$userdata.description"},
                         address:{"$first":"$address"},
                         isActive:{"$first":"$isActive"},
                         isDeleted:{"$first":"$isDeleted"},
                        hospitalName:{"$first":"$hospitalName"},
                        mobile_no:{"$first":"$mobile_no"},
                        createdAt:{"$first":"$createdAt"},
                            city:{"$first":"$city"},
                            email:{"$first":"$email"},
                            _id:"$_id"
                        }
                    },
                    {$sort: sortObject},
                    {
                        $limit: parseInt(skip) + parseInt(count)
                    },
                    {
                        $skip: parseInt(skip)
                    },
                     
                       //count: { $sum: 1 }
                    
                ]).exec(function (err, data) {
                if (data.length) {
                    console.log("data=========",data)
                    Hospital.count(condition).exec(function (err,count) {
                    var totalCount = count
                    res.json({
                        code: constant.statusCode.ok,
                        data: data,
                        message: constant.messages.Hospital_listData,
                        totalCount: totalCount
                    })
                    })

                }
                else {
                    res.json({ code: constant.statusCode.ok, data: [], message: constant.validateMsg.noRecordFound })

                }
            })
        } catch (err) {
            return res.json(Response(500, constant.validateMsg.internalError, err));

        }
    } listHospital().then(function (data) { })
}

