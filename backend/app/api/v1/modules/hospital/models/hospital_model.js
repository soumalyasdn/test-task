'use strict';

var mongoose = require('mongoose');

var hospitalSchema = new mongoose.Schema({
    user_id: { type: mongoose.Schema.Types.ObjectId, ref:'user'},
    hospital_no: { type: String },
    // subject: { type: String, required: true },
    description: { type: String, default: ''},
    isActive: { type: Boolean, default: true },
    isDeleted: { type: Boolean, default: false },
    //created_by_id: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    createdDate: { type: Date, default: Date.now },
    modifiedDate: { type: Date, default: Date.now },
}, {
        timestamps: true
    });

var hospital = mongoose.model('hospital', hospitalSchema);
module.exports = hospital;