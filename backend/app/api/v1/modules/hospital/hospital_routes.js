module.exports = function (router) {

    var utils = require('../../../../lib/util');
    var middlewares = [utils.ensureAuthorized];

    var hospital = require('./controllers/hospital_ctrl');

     router.post('/hospital/addHospital', hospital.addHospital);
     router.post('/hospital/listHospital',middlewares, hospital.listHospital);
    

    return router;
}