﻿'use strict';

const util = require('util'),
    validator = require('../../../../../../app/lib/validator'),
    jwt = require('jsonwebtoken'),
    utility = require('../../../../../../app/lib/utility.js'),
    mailer = require('../../../../../../app/lib/mailer.js'),
    Error = require('../../../../../../app/lib/error.js'),
    Response = require('../../../../../../app/lib/response.js'),
    constant = require('../../../../../../app/lib/constants'),
    query = require('../../../../../../app/lib/common_query'),
    messageTemplates = require("../../../../../lib/messagetemplates"),
    Role = require('../../role/models/role_model'),
    User = require('../models/user_model');

const config = require('../../../../../../app/config/config.js').get(process.env.NODE_ENV);


module.exports = {
    UserRegistration: UserRegistration,
    login: login,
    forgotPassword: forgotPassword,
    userLogout: userLogout,
    resetPassword: resetPassword,
    changePassword: changePassword,
    editAdminProfile: editAdminProfile,
    checkUrl: checkUrl,
    getUserDetails: getUserDetails
};

/**
 * Function is use to login Super admin
 * @access private
 * @return json
 * Created by Vikash Kumar
 * @smartData Enterprises (I) Ltd
 * Created Date 18-mar-2020
 */
function login(req, res) {
    // console.log("login request============", req.body)
    async function loginMethod() {
        try {
            let finalObjectToBeSend = {};
            let jwtToken = null;
            let findDataCondition = {
                $or: [
                    { email: req.body.email },
                    { email: req.body.email },
                ],
                isDeleted: false
            }

            let populateCondition = {
                path: 'role_id'
            }
            let findUserDetails = await query.findoneWithPopulate(User, findDataCondition, populateCondition);
            // console.log("findUserDetailsfindUserDetails", findUserDetails)
            if (findUserDetails.status) {
                if (findUserDetails.data && findUserDetails.data.isActive) {
                    findUserDetails.data.comparePassword(req.body.password, async function (err, isMatch) {
                        if (err) {
                            return res.json(Response(constant.statusCode.internalservererror, constant.validateMsg.invalidEmailOrPassword));
                        }
                        else if (isMatch) {
                            let params = {
                                user_id: findUserDetails.data._id,
                                user_type: findUserDetails.data.role_id.role
                            }
                            let expirationDuration = 60 * 60 * 24 * 15; // expiration duration format sec:min:hour:day. ie: 8 Hours as per i/p
                            jwtToken = jwt.sign(params, constant.cryptoConfig.secret, {
                                expiresIn: expirationDuration
                            });

                            delete findUserDetails.data['password'];

                            finalObjectToBeSend = {
                                token: jwtToken,
                                userInfo: findUserDetails.data,
                                userType: findUserDetails.data.role_id.role
                            }
                            if (finalObjectToBeSend) {
                                return res.json(Response(constant.statusCode.ok, constant.messages.loginSuccess, finalObjectToBeSend));
                            }
                        }
                        else {
                            return res.json(Response(constant.statusCode.internalservererror, constant.messages.newAndoldPwdNotMatch));
                        }
                    });
                } else if (findUserDetails.data && !findUserDetails.data.isActive) {
                    return res.json(Response(constant.statusCode.forbidden, constant.validateMsg.accountIsNotActive));
                }
                else {
                    return res.json(Response(constant.statusCode.unauth, constant.messages.invalidUserNameOrPwd));
                }
            }
            else {
                return res.json(Response(constant.statusCode.internalservererror, constant.validateMsg.internalError));
            }

        }
        catch (error) {
            return res.json(Response(constant.statusCode.internalservererror, constant.validateMsg.internalError, error));
        }
    }
    loginMethod().then(function (data) {
    })
}


/**
 * Function is use to change password
 * @access private
 * @return json
 * Created by vikash kumar
 * @smartData Enterprises (I) Ltd
 * Created Date 19-Mar-2020
 */
function changePassword(req, res) {

    async function changePasswordMethod() {
        if (req.body && req.body.userId && req.body.oldPassword && req.body.newPassword && req.body.newRepassword) {
            if (req.body.newPassword === req.body.newRepassword) {
                let cond = {
                    _id: req.body.userId
                }
                try {
                    var result = await query.findoneData(User, cond);
                    if (result.status) {
                        if (result.data != null) {
                            result.data.comparePassword(req.body.oldPassword, function (err, isMatch) {
                                if (err) {
                                    return res.json(Response(constant.statusCode.internalservererror, constant.validateMsg.internalError));
                                } else if (isMatch) {
                                    result.data.comparePassword(req.body.newPassword, async (err, checkMatch) => {
                                        if (err) {
                                            return res.json(Response(constant.statusCode.internalservererror, constant.validateMsg.internalError));
                                        } else if (checkMatch) {
                                            return res.json(Response(constant.statusCode.unauth, constant.validateMsg.passwordNotMatch));
                                        } else {
                                            let hashValue = await query.saltThePassword(req.body.newPassword);
                                            if (hashValue.status) {
                                                let finalResult = await query.updateOneDocument(User, cond, { password: hashValue.value });
                                                if (!finalResult.status) {
                                                    return res.json(Response(constant.statusCode.internalservererror, constant.validateMsg.internalError, finalResult.error));
                                                }
                                                else {
                                                    return res.json(Response(constant.statusCode.ok, constant.messages.changePasswordSuccess, finalResult.data));
                                                }
                                            } else {
                                                return res.json(Response(constant.statusCode.internalservererror, constant.validateMsg.internalError));
                                            }
                                        }
                                    })
                                } else {
                                    return res.json(Response(constant.statusCode.unauth, constant.validateMsg.invalidOldPassword));
                                }
                            })
                        } else {
                            return res.json(Response(constant.statusCode.internalservererror, constant.validateMsg.userNotFound));
                        }
                    } else {
                        return res.json(Response(constant.statusCode.internalservererror, constant.validateMsg.internalError));
                    }
                } catch (error) {

                    return res.json(Response(constant.statusCode.internalservererror, constant.validateMsg.internalError));
                }
            } else {

                return res.json(Response(constant.statusCode.unauth, constant.messages.newAndoldPwdNotMatch));
            }
        } else {

            return res.json(Response(constant.statusCode.unauth, constant.validateMsg.requiredFieldsMissing));
        }
    }
    changePasswordMethod().then(function (data) { });
}

/**
 * Forgot passsword
 * @access private
 * @return json
 * Created by vikash kumar
 * @smartData Enterprises (I) Ltd
 * Created Date 19-Mar-2020
 */
function forgotPassword(req, res) {
    async function forgot_password() {
        try {
            if (!req.body.email) {
                res.json(
                    Error(
                        constant.statusCode.error,
                        constant.validateMsg.requiredFieldsMissing,
                        constant.validateMsg.requiredFieldsMissing
                    )
                );
            } else if (req.body.email && !validator.isEmail(req.body.email)) {
                res.json(
                    Error(
                        constant.statusCode.error,
                        constant.validateMsg.invalidEmail,
                        constant.validateMsg.invalidEmail
                    )
                );
            } else {
                var model = User;
                var condition = {
                    email: req.body.email,
                    isDeleted: false
                };
                var fields = { firstName: 1, lastName: 1, _id: 1, email: 1, isDeleted: 1, isActive: 1, userName: 1 };
                var userObj = await query.findoneData(model, condition, fields);
                // console.log("userObjuserObj++++++++", userObj)
                if (userObj.data) {
                    var condition = {
                        _id: userObj.data._id
                    };
                    var updateData = {
                        resetkey: ''
                    }
                    updateData.resetkey = utility.uuid.v1();
                    console.log("data for resetkey updateData", updateData);

                    let updateKey = await query.updateOneDocument(model, condition, updateData);
                    if (updateKey.data._id) {
                        var baseUrl = config.baseUrl;
                        var userMailData = {
                            userId: userObj.data._id,
                            email: userObj.data.email ? userObj.data.email : '',
                            firstName: userObj.data.firstName ? userObj.data.firstName : '',
                            lastName: userObj.data.lastName ? userObj.data.lastName : '',
                            userName: userObj.data.userName ? userObj.data.userName : '',
                            link: baseUrl + '#/create-password/' + updateKey.data.resetkey,
                        };
                        let obj = {
                            data: userMailData,
                            mailType: constant.varibleType.FORGET_PASSWORD,  //"Forget Password"
                        }
                        // console.log("data for forget password========", obj)
                        let sendMail = await query.sendEmailFunction(obj);
                        if (sendMail) {
                            console.log("sendMailsendMail==========")
                            res.json(
                                Response(
                                    constant.statusCode.ok,
                                    constant.messages.forgotPasswordSuccess
                                )
                            );
                        }
                        else {
                            res.json(
                                Response(
                                    constant.statusCode.internalservererror,
                                    constant.validateMsg.internalError
                                )
                            );
                        }

                    }
                } else {
                    res.json(
                        Response(
                            constant.statusCode.notFound,
                            constant.validateMsg.emailNotExist
                        )
                    );
                }

            }
        } catch (error) {
            return res.json(Response(constant.statusCode.notFound, constant.validateMsg.notificationNotSent));
        }
    };
    forgot_password().then((data) => {
    });

}


/**
 * reset passsword
 * @access private
 * @return json
 * Created by vikash kumar
 * @smartData Enterprises (I) Ltd
 * Created Date 19-Mar-2020
 */
function resetPassword(req, res) {
    async function reset_password() {
        try {
            if (req.body.password == '' || req.body.resetkey == '') {
                res.json(
                    Error(
                        constant.statusCode.error,
                        constant.resetPassword_message.key_password_required
                    )
                );
            } else {
                var model = User;
                var condition = { resetkey: req.body.resetkey };
                var fields = { _id: 1 };
                var userObj = await query.findoneData(model, condition, fields);
                var userData = userObj.data;
                if (userData._id) {
                    userData.password = req.body.password;
                    userData.resetkey = '';
                    console.log("userData", userData)
                    // userData.save(function (err, data) {
                        userData.save(async function (err, userData) {
                        console.log("data", userData)
                        if (err) {
                            res.json(
                                Error(
                                    constant.statusCode.error,
                                    err
                                )
                            );
                        } else {
                            res.json(
                                Response(
                                    constant.statusCode.ok,
                                    constant.resetPassword_message.password_reset_success,
                                    {}, null
                                )
                            );
                        }
                    })
                } else {
                    res.json(
                        Response(
                            constant.statusCode.ok,
                            constant.resetPassword_message.reset_token_expire,
                            {}, null
                        )
                    );
                }
            }
        } catch (error) {
            return res.json(Response(constant.statusCode.notFound, constant.validateMsg.notificationNotSent));
        }
    }
    reset_password().then((data) => {

    })
}


function userLogout(req, res) {
    async function userLogoutMethod() {
        try {
            // Add logout finction here & send response
            return res.json(Response(constant.statusCode.ok, constant.messages.loginOut));
        }
        catch (error) {
            return res.json(Response(constant.statusCode.internalservererror, constant.validateMsg.internalError, error));
        }
    }
    userLogoutMethod().then(function (data) {
    });
    return
};

/**
 * Register for User and other roles
 * @access private
 * @return json
 * Created by Vikash
 * @smartData Enterprises (I) Ltd
 * Created Date 18-Mar-2020
 */

function UserRegistration(req, res) {
    async function UserRegistration() {
        try {
            var body = req.body;
            console.log("req body========", body)
            if ( !validator.isValid(body.email) || !validator.isValid(body.password) || !body.name) {
                res.json({
                    'code': constant.statusCode.unauth,
                    'message': constant.validateMsg.requiredFieldsMissing
                });
            }
            else {
                let cond = {
                    role: body.role,
                    isDeleted: false
                }
                // let Userdata = await query.findoneData(Role,{ name: 'Manager', isDeleted: false });
                let Userdata = await query.findoneData(Role, cond);

                if (!Userdata.status) {
                    res.json({
                        'code': constant.statusCode.unauth,
                        'message': constant.messages.commonError
                    });
                }
                else {
                    if (Userdata) {
                        // console.log("Userdata================", Userdata)


                        let matchemail = await query.findoneData(User, { email: body.email });
                        console.log("matchemail", matchemail)
                        if (matchemail.data) {
                            res.json({
                                'code': constant.statusCode.unauth,
                                'message': constant.messages.ALREADY_REGISTERD
                            });
                        }
                        else if (!matchemail.data) {
                            console.log("email not match");
                            
                            var userObj = {
                                //zipcode: req.body.zipcode ? req.body.zipcode : '',
                                email: req.body.email ? req.body.email.trim().toLowerCase() : '',
                                //fax: req.body.fax ? req.body.fax : '',
                                password: req.body.password ? req.body.password : '',
                                mobile_no: req.body.mobileNumber ? req.body.mobileNumber : '',
                                name:body.name,
                                //isActive: req.body.addedByAdmin ? req.body.addedByAdmin : false,
                                role_id: Userdata.data._id ? Userdata.data._id : ''
                            };
                            let mailKeyword = '';//= constant.emailKeyword.registration;

                            var saveObj = await query.uniqueInsertIntoCollection(User, userObj);
                            console.log("USER DATA ", saveObj);
                            if (!saveObj.status) {
                                console.log("USER DATA 1", saveObj.err);
                                return res.json(Response(constant.statusCode.internalservererror, constant.validateMsg.internalError, saveObj.err));
                            } else {
                                console.log("USER DATA 2 ", saveObj);

                                //newly_inserted_userDetails = saveObj.userData
                                res.json({
                                    code: constant.statusCode.ok,//200,
                                    message: constant.messages.Registration,
                                    data: saveObj.userData
                                })
                            }



                        }
                        else {
                            res.json({
                                'code': constant.statusCode.unauth,
                                'message': constant.messages.commonError
                            });
                        }
                    }
                }
            }
        } catch (error) {
            return res.json(Response(constant.statusCode.notFound));

        }
    }
    UserRegistration().then(function (data) { })
}

/**
 * Register for User and other roles
 * @access private
 * @return json
 * Created by Vikash
 * @smartData Enterprises (I) Ltd
 * Created Date 18-Mar-2020
 */

function editAdminProfile(req, res) {
    async function editProfile() {
        try {
            console.log("req body=========", req.body)
            let userObj = {
                firstName: req.body.firstName ? req.body.firstName : '',
                lastName: req.body.lastName ? req.body.lastName : '',
                email: req.body.email ? req.body.email : '',
                mobile_no: req.body.mobileNumber ? req.body.mobileNumber : '',
                address: req.body.address ? req.body.address : '',
                city: req.body.city ? req.body.city : '',
            }
            let cond = {
                _id: req.body.user_id
            }
            let updateProfile = await query.updateOneDocument(User, cond, userObj)
            if (!updateProfile.status) {
                return res.json(Response(constant.statusCode.internalservererror, constant.validateMsg.internalError, result.error));
            } else {
                return res.json(Response(constant.statusCode.ok, constant.messages.admindata_updated));
            }
        } catch (error) {
            return res.json(Response(constant.statusCode.internalservererror, constant.validateMsg.internalError + " " + error));

        }
    }
    editProfile().then(function (data) { })
}

/**
 * CheckUrl API 
 * @access private
 * @return json
 * Created by vikash kumar
 * @smartData Enterprises (I) Ltd
 * Created Date 19-Mar-2020
 */
function checkUrl(req, res) {
    async function checkUrlMethod() {
        try {
            if (req.body && req.body.resetkey) {
                let dataToFind = {
                    // email : req.body.email,
                    resetkey: req.body.resetkey,
                    isDeleted: false,
                    isActive: true
                }

                let findEntry = await query.findoneData(User, dataToFind);
                if (findEntry.status) {
                    if (findEntry.data) {
                        return res.json(Response(constant.statusCode.ok, constant.messages.validUrl));
                    }
                    else {
                        return res.json(Response(constant.statusCode.notFound, constant.messages.invalidUrl));
                    }
                }
                else {
                    return res.json(Response(constant.statusCode.internalservererror, constant.validateMsg.internalError));
                }

            }
            else {
                return res.json(Response(constant.statusCode.unauth, constant.validateMsg.requiredFieldsMissing));
            }
        }
        catch (err) {
            return res.json(Response(500, constant.validateMsg.internalError, err));
        }
    };
    checkUrlMethod().then((data) => {
    });

}

/**
 * getUserDetails API 
 * @access private
 * @return json
 * Created by vikash kumar
 * @smartData Enterprises (I) Ltd
 * Created Date 23-Mar-2020
 */

function getUserDetails(req, res) {
    async function listByIdAdminExe() {
        try {
            let Condition = {
                _id: mongoose.Types.ObjectId(req.body.user_id)
            }
            let AdminUserData = await query.findoneData(User, Condition);

            if (AdminUserData.status == true && AdminUserData.data) {
                return res.json(Response(200, constant.messages.User_getData, AdminUserData.data));
            }
            else {
                return res.json(Response(500, constant.validateMsg.internalError, result.error));
            }
        }
        catch (err) {
            return res.json(Response(500, constant.validateMsg.internalError, err));
        }

    }
    listByIdAdminExe().then(function (data) { });
}
