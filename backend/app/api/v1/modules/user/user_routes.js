module.exports = function (router) {

    var utils = require('../../../../lib/util');
    var middlewares = [utils.ensureAuthorized];

    var user = require('./controllers/user_ctrl');

    router.post('/user/login', user.login);
    router.post('/user/forgotPassword', user.forgotPassword);
    router.post('/user/userLogout', user.userLogout);
    router.post('/user/UserRegistration', user.UserRegistration);
    router.post('/user/resetPassword', user.resetPassword);
    router.post('/user/changePassword', user.changePassword);
    router.post('/user/editAdminProfile', user.editAdminProfile);
    router.post('/user/checkUrl', user.checkUrl);
    router.post('/user/getUserDetails',middlewares,user.getUserDetails);
    return router;
}