'use strict';

const mongoose = require('mongoose');
const constantsObj = require('../../../../../lib/constants');
const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;

const userSchema = new mongoose.Schema({

    role_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Role' },          // reference with roles
    email: { type: String, lowercase: true, unique: true },  //unique: true,
    password: { type: String, default: null },
    name:{type:String,required:true},
    hospitalName:{type:String,default:''},
    mobile_no: { type: String,default:'' },
    isDeleted: { type: Boolean, default: false },
    isActive: { type: Boolean, default: true },
    contacted: {type: String,default: null},
    resetkey: { type: String, default: '' }
}, {
        timestamps: true
    });

userSchema.statics.existCheck = function (email, id, callback) {
    var where = {};
    if (id) {
        where = {
            $or: [{ email: new RegExp('^' + email + '$', "i") }], deleted: { $ne: true }, _id: { $ne: id }
        };
    } else {
        where = { $or: [{ email: new RegExp('^' + email + '$', "i") }], deleted: { $ne: true } };
    }
    User.findOne(where, function (err, userdata) {
        if (err) {
            callback(err)
        } else {
            if (userdata) {
                callback(null, constantsObj.validateMsg.emailAlreadyExist);
            } else {
                callback(null, true);
            }
        }
    });
};

userSchema.pre('save', function (next) {
    var user = this;
    if (!user.isModified('password')) return next();
    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
        if (err) return next(err);
        bcrypt.hash(user.password, salt, function (err, hash) {
            if (err) return next(err);
            user.password = hash;
            next();
        });
    });
});

userSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

var user = mongoose.model('user', userSchema);
module.exports = user;

