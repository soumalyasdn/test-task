const statusCode = {
    "ok": 200,
    "unauth": 401,
    "notFound": 404,
    "validation": 400,
    "failed": 1002,
    "invalidURL": 1001,
    "paymentReq": 402,
    "internalError": 1004,
    "forbidden": 403,
    "internalservererror": 500,
    "alreadyExist": 409 //conflict
}
const messages = {
    "success":"success",
    "user_busy":"User is already Busy",
    "user_available":"Available to connect",
    "commonError": "Something went wrong",
    'ALREADY_REGISTERD':'Email already register',
    'PHONE_ALREADY_REGISTERD':'Phone number already registered',
    "loginSuccess": "Logged in successfully",
    "signupSuccess": "Signup successfully, please check you email to activate your account",
    "loginOut": "Logout successfully",
    "Registration": "You have successfully registered",
    "emailAlreadyExist": "Email already exist",
    "requestAccepted": "Request accepted successfully",
    "newAndoldPwdNotMatch": "Password not match",
    "forgotPasswordSuccess": "Reset link send to your email",
    "hospitalWelcomeSuccess": "Welcome mail send to the registered email",
    "createPasswordSuccess": "Password created sucessfully.",
    "validUrl": "Valid Url",
    "invalidUrl": "Invalid Url",
    "userNotFound": "User not found",
    "userDeleteSuccess": "User archived sucessfully",
    "invalidUserNameOrPwd":"Invalid user name or password",
    "admindata_updated":"Profile updated successfully",
    "User_getData":"User details fetched successfully",
    "Hospital_listData":"Health Care Facility list fetched successfully",
    "Hospital_updated":"Health Care Facility updated successfully",
    "ActivatedSucess": "Status activated successfully.",
    "DeActivatedSucess": "Status de-activated successfully.",
    "Patient_listData":"Patient list fetched successfully",
    "Patient_Edit":"Patient edited successfully",
    "Patient_details":"Patient details fetched successfully",
    "video_uploaded":"video uploaded successfully",
    "notes_added_successfully":"notes added successfully",
    "notes_edited_successfully":"notes edited successfully",
    "Callstatus":"Call status changed successfully",
    "Psychiatrist_listData":"Mental Health Provider list fetched successfully",
    "Psychiatrist_details":"Mental Health Provider details fetched successfully",
    "Psychiatrist_Edit":"Mental Health Provider edited successfully",
    "Psychiatrist_assign":"Mental Health Provider assign successfully",
    "Psychiatrist_nodata":"No Mental Health Provider alligned, Please add!",
    "ActivatedSucessArchive": "data de-archived successfully.",
    "Icp_listData":"Icp list fetched successfully",
    "Icp_Edit":"Icp edited successfully",
    "Icp_details":"Icp details fetched successfully",
    "dashboardHospitalCount":"dashboard data fetched successfully",
    "treatment_added_successfully":"Treatment plan added successfully",
    "treatment_edited_successfully":"Treatment plan edited successfully",
    "treatment_lock_successfully":"Treatment plan locked successfully",
    "notes_successfully":"Final notes added successfully",
    "doses_success":"Fetched doses successfully",
    "doses_error":"All doses could not be fetched"
}
const resetPassword_message = {
    "key_password_required": "Reset key or password is missing.",
    "reset_token_expire": "Reset password token expires! Regenerate token to set password",
    "password_reset_success": "Password reset successfully.Please try to login."
}
const validateMsg = {
    "internalError": "Internal error",
    "requiredFieldsMissing":"required field are missing",
    "invalidEmail": "Invalid Email Given",
    "accountIsNotActive":"account is not active"
}

const cryptoConfig = {
    "cryptoAlgorithm": "aes-256-ctr",
    "cryptoPassword": '@#DialogflowVoiceBasedBot#@',//'d6F3Efeq',
    "secret": "!@#$PatientEngagement!@#$"//"Test2019",
}
const varibleType = {
    "FORGET_PASSWORD":"Forget Password",
    "MANAGER_WELCOME":"manager_welcome",
    "SPECIALIST": "Specialist",
    "PATIENT": "Patient",
    "HOSPITAL":"Hospital Admin",
    "PSYCHIATRIST":"Psychiatrist",
    "ICP":"Initial Care Provider"
}
const emailKeyword = {
    "registration": "registration",
    "forgotPassword": "forgot_password",
    "maangerWelcome":"manager_welcome",
    "hospitalWelcome":"hospital_welcome",
    "patientWelcome":"patient_welcome",
    "psychiatristWelcome":"psychiatrist_welcome",
    "icpWelcome":"icp_welcome"
}
const userTypes = {
    admin: "Admin",
    manager: "Manager"
}
const directoryPath = { 
    //"SERVICEIMAGE": "../backend/app/uploads/",//local
    //"SERVICEIMAGE":"/HealMed/admin/backend/app/uploads/",
    "SERVICEIMAGE":"/home/jenkins/workspace/SDN_PatientEngagement/backend/app/uploads/",
    "EXTENSIONS":["docx", "doc", "pdf", "xls","xlsx"],
    "ALLOWED":"docx, doc, pdf, xls,xlsx"
}
const obj = {
    cryptoConfig: cryptoConfig,
    statusCode: statusCode,
    messages:messages,
    validateMsg: validateMsg,
    varibleType:varibleType,
    userTypes:userTypes,
    emailKeyword:emailKeyword,
    directoryPath:directoryPath,
    resetPassword_message:resetPassword_message
};

module.exports = obj;