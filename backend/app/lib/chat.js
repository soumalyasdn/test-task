"use strict";

const mongoose = require("mongoose");
const chatRoomModel = require("../api/v1/modules/chat/models/chatroom");
const chatmessageModel = require("../api/v1/modules/chat/models/chatmessage");
const commonQuery = require("./common_query");
const Response = require("./response");
const constant = require("./constants");

module.exports = {
  socketMethod: socketMethod,
  getAllMessage: getAllMessage
  //attachImage: attachImage,
};

// function attachImage(req, res) {
//   async function attachImage() {
//     try {
//       if (req.files && req.body && req.body.roomId) {
//         let imagePath = await commonQuery.fileUpload(
//           Math.random(new Date()) + req.files["file"]["name"],
//           req.files["file"]["data"]
//         );
//         if (imagePath.status) {
//           let chatObj = {
//             message: {
//               _id: mongoose.Types.ObjectId(req.body._id),
//               createdAt: new Date(),
//               image: imagePath.url,
//               user: JSON.parse(req.body.user),
//             },
//           };
//           imagePath.createdAt = chatObj.message.createdAt;
//           imagePath._id = chatObj.message._id;
//           imagePath.image = chatObj.message.image;
//           imagePath.imageType = req.body.imageType;
//           imagePath.imageName = req.files["file"]["name"];
//           if (imagePath) {
//             res.json(
//               Response(
//                 constant.SUCCESS_CODE,
//                 constant.UPDATE_SUCCESS,
//                 imagePath
//               )
//             );
//           }
//         } else {
//           res.json(
//             Response(constant.ERROR_CODE, constant.FAILED_TO_PROCESS, null)
//           );
//         }
//       }
//     } catch (error) {
//       res.json(
//         Response(constant.ERROR_CODE, constant.REQUIRED_FIELDS_MISSING, null)
//       );
//     }
//   }
//   attachImage().then(function () {});
// }

function socketMethod(io) {
  async function socketMethod() {
    try {
      // const io = require("socket.io").listen(server);

      // const io = req.app.get("io")

      io.on("connection", async (socket) => {
        console.log("Connection established");
        socket.on("join", async (data) => {
          let querObj = {
            from_id: {
              $in: [data.from_id, data.to_id],
            },
            to_id: {
              $in: [data.from_id, data.to_id],
            },
          };

          let room = await commonQuery.findAll(chatRoomModel, querObj);
          //contacted update data
          let contactcondition = {
            _id: mongoose.Types.ObjectId(data.from_id)//,
            //userType: "patient",
          };

          let dataToUpdate = {
            contacted: "chat",
          };

          const updateUserDetail = await commonQuery.updateOne(
            users,
            contactcondition,
            dataToUpdate
          );
          //contacted update data end
          let roomId = "";
          if (room.length == 0) {
            let insersetRoom = await commonQuery.InsertIntoCollection(
              chatRoomModel,
              data
            );
            roomId = JSON.parse(JSON.stringify(insersetRoom._id));
          }
          if (room.length == 1) {
            roomId = JSON.parse(JSON.stringify(room[0]._id));
            let chatCond = {
              room_id: roomId,
            };
            let chatHistory = await commonQuery.findoneData(
              chatmessageModel,
              chatCond
            );
            socket.emit("chat History", chatHistory);
          } else {
            console.log("error while getting room id");
          }
          if (roomId) {
            socket.join(roomId);
            socket.emit("room Id", roomId);
            socket.broadcast.to(roomId).emit("new user joined", {
              user: roomId,
              message: "has joined",
            });
          }
        });
        socket.on("leave", (data) => {
          socket.broadcast
            .to(data.room)
            .emit("left room", { user: data.user, message: "has left" });
          socket.leave(data.room);
        });
        socket.on("message", async (data) => {
          // let type = data.type || "web";
          // let chatObj = {
          //     message:{
          //     _id:mongoose.Types.ObjectId(data._id),
          //     createdAt:new Date(),
          //     text:data.text||null,
          //     image:data.image||null,
          //     imageType:data.imageType||null,
          //     imageName:data.imageName||null,
          //     user:data.user
          // }
          // }
          // let checkObj={
          //     room_id:mongoose.Types.ObjectId(data.roomId)
          // }
          // let result = await commonQuery.pushChatMessage(chatmessageModel,checkObj,chatObj);
          // let messageObj = {
          //     _id:data._id,
          //     text:data.text||null,
          //     image:data.image||null,
          //     imageType:data.imageType||null,
          //     imageName:data.imageName||null,
          //     createdAt:new Date(),
          //     user:data.user,
          //     roomId:data.roomId
          // }
          //     io.in(data.roomId).emit("new message",messageObj);

          if (data && data.roomId) {
            let checkObj = {
              room_id: mongoose.Types.ObjectId(data.roomId),
            };
            let chatObj = {
              message: {
                data: data.data,
              },
            };

            let result = await commonQuery.pushChatMessage(
              chatmessageModel,
              checkObj,
              chatObj
            );

            io.in(data.roomId).emit("new message", data.data);
          }
        });
      });
    } catch (err) {
      console.log("error is socketMethod");
    }
  }
  socketMethod().then((res) => {});
}

function getAllMessage(req, res) {
  async function getAllMessage() {
    try {
      if (req.body.user_id) {
        let userChatMessage = await chatRoomModel
          .aggregate([
            {
              $match: {
                $or: [
                  { to_id: mongoose.Types.ObjectId(req.body.user_id) },
                  { from_id: mongoose.Types.ObjectId(req.body.user_id) },
                ],
              },
            },
            {
              $lookup: {
                from: "chatmessages",
                let: { roomId: "$_id" },
                pipeline: [
                  { $match: { $expr: { $eq: ["$room_id", "$$roomId"] } } },
                ],
                as: "chatmessage",
              },
            },
            { $unwind: "$chatmessage" },
            {
              $project: {
                user1_name: 1,
                user2_name: 1,
                from_id: 1,
                to_id: 1,
                roomId: "$chatmessage.room_id",
                lastMessage: { $arrayElemAt: ["$chatmessage.message", -1] },
                updatedAt: "$chatmessage.updatedAt",
              },
            },
          ])
          .exec();

        if (userChatMessage && userChatMessage.length) {
          userChatMessage.map((element) => {
            let user1 = element.from_id.toString();
            let user2 = element.to_id.toString();
            if (req.body.user_id != user1) {
              element.toUserId = user1;
              element.toUserName = element.user1_name;
              delete element.user1_name;
              delete element.from_id;
              delete element.to_id;
              delete element.user2_name;
            }
            if (req.body.user_id != user2) {
              element.toUserId = user2;
              element.toUserName = element.user2_name;
              delete element.user1_name;
              delete element.from_id;
              delete element.to_id;
              delete element.user2_name;
            }
          });
          res.json(
            Response(
              constant.ok,
              "Received chatts with success",
              userChatMessage
            )
          );
        } else {
          res.json(Response(constant.failed,"Failed to process response"));
        }
      }
    } catch (err) {
      console.log("Error in getAllMessage");
    }
  }
  getAllMessage().then((response) => {});
}
