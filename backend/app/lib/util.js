'use strict';
// var jwt = require('jsonwebtoken'),
//     constant = require('./constants');
var jwt = require('jsonwebtoken'),
    constant = require('./constants');
const jwtKey = '!@#$PatientEngagement!@#$'//'Test2019'
var allowed = [
    '/user/userRegister',
    '/user/verifyAccount',
    '/user/userLogin',
    '/user/forgotPassword',
    '/user/resetPassword',
];

module.exports = {
    ensureAuthorized: ensureAuthorized
}
function ensureAuthorized(req, res, next) {
  var bearerToken;
  var bearerHeader = req.headers["authorization"];
  if (bearerHeader !== "undefined") {
    // console.log("BEARER HEADER", bearerHeader);
    var bearer = bearerHeader.split(" ")//bearerHeader; //bearerHeader.split(" ");

    // console.log("sss", bearer[1]);
    bearerToken = bearer[1];
    // console.log("SSSSS", bearerToken);
    jwt.verify(bearerToken, jwtKey, function(err, decoded) {
      // console.log("decoded ================= ", err, decoded);
      req.user = decoded;
      if (err) {
        return res.send({
          code: 400,//Constant.AUTH_CODE,
          message: 'Invalid token'//Constant.INVALID_TOKEN
        });
      }
      next();
    });
  } else {
    return res.send({
      code: 400,//Constant.AUTH_CODE,
      message: 'Token error'//Constant.TOKEN_ERROR
    });
  }
}

// function ensureAuthorized(req, res, next) {
//   console.log("requuuuuuuu",req)
//     if (req.headers.authorization !== "undefined") {
//       jwt.verify(req.headers.authorization, constant.cryptoConfig.secret, function(err, decoded) {
//         req.user = decoded;
//         if (err) {
//           return res.send({
//             code: 400,//Constant.AUTH_CODE,
//             message: 'Invalid token'//Constant.INVALID_TOKEN
//           });
//         }
//         next();
//       });
//     } else {
//       return res.send({
//         code: 400,//Constant.AUTH_CODE,
//         message: 'Token error'//Constant.TOKEN_ERROR
//       });
//     }
//   }