'use strict';
var mongoose = require('mongoose');

//All models schema
__rootRequire("app/api/v1/modules/user/models/user_model");
__rootRequire("app/api/v1/modules/v_chat/models/archive_model");

const config = require('./config.js').get(process.env.NODE_ENV);
// console.log("DB cred----",config.db.url,config.db.user,config.db.password,process.env.NODE_ENV)
mongoose.Promise = global.Promise;
mongoose.connect(config.db.url, { user: config.db.user, pass: config.db.password ,useNewUrlParser: true,useUnifiedTopology: true });//SDN Local server
var db = mongoose.connection;
db.on('error', console.error.bind(console, "connection failed"));
db.once('open', function () {
	console.log("Database conencted successfully!");
});
// mongoose.set('debug', true);




