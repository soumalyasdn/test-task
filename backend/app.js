'use strict';

var express = require('express');
var fs = require('fs');
var bodyParser = require('body-parser');
var path = require('path');
var https = require('https');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./app/swagger/swagger.json');
var cors = require('cors');

global.__rootRequire = function(relpath) {
    return require(path.join(__dirname, relpath));
};

var app = express();
app.use(cors());

app.use('/apiDocs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

//  process.env.NODE_ENV = process.env.NODE_ENV || 'local'; //local server
process.env.NODE_ENV = process.env.NODE_ENV || 'staging'; //staging servers
//process.env.NODE_ENV = process.env.NODE_ENV || 'prod'; //local server


const config = require('./app/config/config.js').get(process.env.NODE_ENV);
require('./app/config/db');
const fileUpload = require('express-fileupload');

var utils = require('./app/lib/util')

app.set('view engine', 'html');
app.set('views', __dirname + '/views');
app.use(fileUpload());
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));

// routes
//app.use('/uploads', express.static(path.join(__dirname, './app/uploads')));
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public/modules/dashboard')));
app.use(express.static(path.join(__dirname, 'public/dist')));
// All api requests
app.use(function(req, res, next) {
    // CORS headers
    res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    // Set custom headers for CORS
    res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key,If-Modified-Since,Authorization,multipart/form-data');

    if (req.method == 'OPTIONS') {
        res.status(200).end();
    } else {
        next();
    }
});


app.use('/api/v1', require('./app/api/v1/routes')(express));

// start server
// var port = process.env.PORT || config.port;
var port = process.env.PORT || 5029;
// app.listen(port).timeout = 1800000; //30 min
console.log("Available on:", config.backendBaseUrl)
console.log("Started");

// New configuration for ssl

var socketServer
var server;
if (config.env == 'local') {
    socketEvents = require('http').createServer();
    server = require('http').createServer(app);
    //  app.set('trust proxy', true)
} else if (config.env == 'staging') {
    const httpsOptions = {
        // cert: fs.readFileSync('/home/jenkins/SSL/mean.crt', 'utf8'),
        // key: fs.readFileSync('/home/jenkins/SSL/mean.key', 'utf8'),
        // cert: fs.readFileSync('/home/jenkins/SSL/meanb.crt', 'utf8'),
    }
    socketEvents = https.createServer( app);
    server = https.createServer( app);

    // app.set('trust proxy', true)
}
var io = require("socket.io").listen(server);
app.set("io", io);
require("./app/lib/chat").socketMethod(io)
// global._socketio = require("socket.io")(socketServer);
// // console.log("socket server>>>>>>>>>>>>>>>>",socketServer);
// socketServer.listen(5983);
server.listen(port).timeout = 1800000;
// require("./app/api/v1/socketio")(_socketio);

// var server = app.listen(port);
var socketEvents = require('./app/api/v1/modules/v_chat/controllers/v_chat_ctrl.js').socketEvents;
var io = require('socket.io').listen(server);
socketEvents(io);